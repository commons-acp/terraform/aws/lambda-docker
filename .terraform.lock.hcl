# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/allence-tunisie/atn-utils" {
  version = "1.0.0"
  hashes = [
    "h1:zSlrx5mXBMhUvM73re411md85IncuQZpjq1FUsuHbww=",
    "zh:3526f87d18a17c898df67987b1ba44aa08ead09d657734daaeb2c28aca0d1a37",
    "zh:505499207a7bdd61abe8be7f9a9021f83d7dbd4c4b730016d7a5a1e7c61a6653",
    "zh:67f2fc4bdad7aed2260a42ec28123428c70515e18d1fc2a9ac6c34c954456149",
    "zh:7e28eda2e185d888abdc45c6188858af478eff1be0ac88176e2a2f237f203e69",
    "zh:b0df5fbf74693fecf27f5da3a2bce24f8e16a94032894f26282f3d22ce706a0a",
    "zh:c4008acfbfa85e8c57f2ea7ced4be63d331dcb17d9c37f62773931632fb421e7",
    "zh:d314660b29405d166bfa247e5b59124ea0c2b55edcb8661779d6ab782702199c",
    "zh:d9fefbc6caaeda8158d6daa13dd9c5bfdf81e538988ddb5fbce6a06f543ef1e9",
    "zh:dbd7a61eb7c5293769c1dfae08c87ffe97a25b2a08b758cb5e17e74a1ffc462c",
    "zh:e68607a5f7cc1b7f79707af7fb73e9c95821b5ce01850be15bd95f7e301a8147",
    "zh:f36f77e1e6b65e623b9da8109bbd633fbb1159cbfcf45bfaabb6cb11b12720a8",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.71.0"
  constraints = "> 3.0.0"
  hashes = [
    "h1:5+M8SPZlb3FxcmAX4RykKzNrTHkpjoP1UpHcenOXcxo=",
    "zh:173134d8861a33ed60a48942ad2b96b9d06e85c506d7f927bead47a28f4ebdd2",
    "zh:2996c8e96930f526f1761e99d14c0b18d83e287b1362aa2fa1444cf848ece613",
    "zh:43903da1e0a809a1fb5832e957dbe2321b86630d6bfdd8b47728647a72fd912d",
    "zh:43e71fd8924e7f7b56a0b2a82e29edf07c53c2b41ee7bb442a2f1c27e03e86ae",
    "zh:4f4c73711f64a3ff85f88bf6b2594e5431d996b7a59041ff6cbc352f069fc122",
    "zh:5045241b8695ffbd0730bdcd91393b10ffd0cfbeaad6254036e42ead6687d8fd",
    "zh:6a8811a0fb1035c09aebf1f9b15295523a9a7a2627fd783f50c6168a82e192dd",
    "zh:8d273c04d7a8c36d4366329adf041c480a0f1be10a7269269c88413300aebdb8",
    "zh:b90505897ae4943a74de2b88b6a9e7d97bf6dc325a0222235996580edff28656",
    "zh:ea5e422942ac6fc958229d27d4381c89d21d70c5c2c67a6c06ff357bcded76f6",
    "zh:f1536d7ff2d3bfd668e3ac33d8956b4f988f87fdfdcc371c7d94b98d5dba53e2",
  ]
}
