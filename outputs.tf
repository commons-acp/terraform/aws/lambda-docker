# Outputs LAMBDA
output "lambda_api_arn" {
  value = aws_lambda_function.lambda.arn
}
output "lambda_api_invoke_arn" {
  value = aws_lambda_alias.api_alias.invoke_arn
}
output "lambda_api_alias" {
  value = aws_lambda_alias.api_alias.name
}

output "lambda_api_function_name" {
  value = aws_lambda_function.lambda.function_name
}

