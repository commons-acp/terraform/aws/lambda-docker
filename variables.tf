#fondation

variable "lambda_role_arn" {
  description = "Lambda Role execution ARN"
}
#project variable
variable "function_name" {
  description = "Lambda Name"
}
variable "handler" {
  default = "index.handler"
  description = "the lambda handler"
}
variable "runtime" {
  description = "the lambda runtime"
  default = "nodejs12.x"
}

variable "image_uri" {
  description = "The image URI of lambda"
  type    = string
}
variable "command" {
  type    = list(string)
  description = "The commande of the container"
}
variable "entry_point" {
  type    = list(string)
  description = "Entry point"
}
variable "working_directory" {
  description = "the working directory of container"
  type    = string
}
variable "lambda_env" {
    description = "variable d'environnement utilisé par lambda "
}
variable "package_type" {
  description = "type de package"
  type    = string
}
variable "memory" {
  description = "memory of image docker"
  type    = string
}