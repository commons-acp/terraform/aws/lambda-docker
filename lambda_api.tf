

resource "aws_lambda_function" "lambda" {
  function_name = var.function_name
  role          = var.lambda_role_arn
  handler       = var.handler
  image_uri = var.image_uri
  package_type = var.package_type
  memory_size = var.memory
  image_config {
    command = var.command
    entry_point = var.entry_point
    working_directory = var.working_directory
  }


  runtime = var.runtime
  timeout = 50
  environment {
    variables = var.lambda_env
  }
  publish=true
}
resource "aws_lambda_alias" "api_alias" {
  name             = "developement"
  description      = "Alias for developement"
  function_name    = aws_lambda_function.lambda.arn
  function_version = "$LATEST"
  depends_on = [aws_lambda_function.lambda]
}
